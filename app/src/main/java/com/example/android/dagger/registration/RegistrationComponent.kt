package com.example.android.dagger.registration

import com.example.android.dagger.di.ActivityScope
import com.example.android.dagger.registration.enterdetails.EnterDetailsFragment
import com.example.android.dagger.registration.termsandconditions.TermsAndConditionsFragment
import dagger.Subcomponent

/*
 *Los subcomponente son componentes que heredan y extienden el gráfico
 *de objetos de un componente de matriz. Por lo tanto, todos los objetos
 *proporcionados en el componente principal también se proporcionarán
 *en el subcomponente. De esta manera, un objeto de un subcomponente
 *puede depender de un objeto proporcionado por el componente padre.
 */

// Scope annotation that the RegistrationComponent uses
// Classes annotated with @ActivityScope will have a unique instance in this Component
@ActivityScope
//Definicion de un subcomponente de dagger
@Subcomponent
interface RegistrationComponent {

    // Factory para crear instancia de RegistrationComponent
    @Subcomponent.Factory
    interface Factory {
        fun create(): RegistrationComponent
    }

    //clases que pueden ser inyectadas por este componente
    fun inject (activity: RegistrationActivity)
    fun inject (fragment: EnterDetailsFragment)
    fun inject (fragment: TermsAndConditionsFragment)

}