package com.example.android.dagger.di

import com.example.android.dagger.storage.SharedPreferencesStorage
import com.example.android.dagger.storage.Storage
import dagger.Binds
import dagger.Module

/*
* @Module: especifica a dagger que esta clase es un modulo dagger
*/
@Module
abstract class StorageModule {

    /*
    * @Binds: Decimos a dagger que implementacion necesita usar al proporcionar
    * una interfaz.
    * @Binds debe anotar una funcion abstracta, El tipo de retorno de la función
    * abstracta es la interfaz para la que queremos proporcionar una implementación.
    * La implementación se especifica agregando un parámetro único con el tipo de
    *  implementación de interfaz
    */
    @Binds
    abstract fun provideStorage(storage: SharedPreferencesStorage): Storage

}