package com.example.android.dagger.di

import android.content.Context
import com.example.android.dagger.main.MainActivity
import com.example.android.dagger.registration.RegistrationActivity
import com.example.android.dagger.registration.RegistrationComponent
import com.example.android.dagger.registration.enterdetails.EnterDetailsFragment
import com.example.android.dagger.registration.termsandconditions.TermsAndConditionsFragment
import dagger.BindsInstance
import dagger.Component
import org.w3c.dom.DocumentFragment
import javax.inject.Singleton

/*
* Una @Component interfaz proporciona la información que Dagger
* necesita para generar el gráfico en tiempo de compilación.
* El parámetro de los métodos de interfaz define qué clases
* solicitan la inyección.
 */

//Definición de un componente Dagger que agrega información del StorageModule al gráfico
@Singleton
@Component(modules = [StorageModule::class, AppSubcomponents::class])
interface AppComponent {

    //Factory para crear instancias del AppComponent
    @Component.Factory
    interface Factory {
        /*
        *@BindsInstance le dice a Dagger que necesita agregar esa instancia en el
        * gráfico y siempre que Context sea ​​necesario, proporcione esa instancia.
        *
        * Usamos @BindsInstance para objetos que se construyen fuera del
        * gráfico (por ejemplo, instancias de Context)
        */
        fun create(@BindsInstance context: Context): AppComponent
    }


    //clases que pueden ser inyectadas por este componente

    //Exponer la fabrica de RegistrationComponent del grafico
    fun registrationComponent (): RegistrationComponent.Factory
    fun inject (activity: MainActivity)


}


/*IMPORTANTE:
Hay dos formas diferentes de interactuar con el gráfico Dagger:

1. Declarar una función que devuelve Unity toma una clase como parámetro
 permite la inyección de campo en esa clase por ejemplo:
 fun inject(activity: MainActivity)

2. Declarar una función que devuelve un tipo permite recuperar tipos
 del gráfico por ejemplo:
 fun registrationComponent(): RegistrationComponent.Factory).

 */