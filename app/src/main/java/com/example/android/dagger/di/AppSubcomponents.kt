package com.example.android.dagger.di

import com.example.android.dagger.registration.RegistrationComponent
import dagger.Module

// Este módulo le dice a AppComponent cuáles son sus subcomponentes
@Module(subcomponents = [RegistrationComponent::class])
class AppSubcomponents
